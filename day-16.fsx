let input = "10111100110001111" |> Seq.toList

let step a =
  let b = a |> List.rev |> List.map (fun x -> if x = '1' then '0' else '1')
  a @ ['0'] @ b

let rec expand targetLen input =
  if input |> List.length >= targetLen then
    input |> List.take targetLen
  else
    input |> step |> expand targetLen

let rec checksum input =
  if (input |> List.length) % 2 = 1 then
    input |> List.map string |> List.reduce (+)
  else
    input |> List.chunkBySize 2 |> List.map (fun x -> if x.[0] = x.[1] then '1' else '0') |> checksum

printfn "Part 1: %s" (input |> expand 272 |> checksum)
printfn "Part 2: %s" (input |> expand 35651584 |> checksum)
