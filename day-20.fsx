let maxIP = int64 System.UInt32.MaxValue

let ranges =
  System.IO.File.ReadAllLines "day-20-input.txt"
  |> Array.map ((fun x -> x.Split '-') >> (fun x -> int64 x.[0], int64 x.[1]))
  |> Array.sort

let rec lowestAllowed ip =
  match ranges |> Seq.tryFind (fun (x, y) -> ip >= x && ip <= y) with
  | Some (_, max) -> lowestAllowed (max + 1L)
  | None -> ip

let rec allowedIPCount ip =
  if ip > maxIP then
    0L
  else
    match ranges |> Seq.tryFind (fun (x, y) -> ip >= x && ip <= y) with
    | Some (_, max) -> allowedIPCount (max + 1L)
    | None ->
      let nextStart =
        ranges
        |> Seq.map fst
        |> Seq.tryFind (fun x -> x > ip)
        |> Option.defaultValue (maxIP + 1L)
      (nextStart - ip) + allowedIPCount nextStart

printfn "Part 1: %d" (lowestAllowed 0L)
printfn "Part 2: %d" (allowedIPCount 0L)
