(*

I solved this problem by analyzing the input and breaking it down into pseudocode. After a couple levels of
breaking down, it looks like this:

d := a + (231 * 11)

while true {
  a := d
  do {
    b := a
    a := 0

    while true {
      c := 2
      do {
        if (b = 0) goto loop_done
        b--
        c--
      } while (c != 0)
      a++
    }

    loop_done:
    output (2 - c)

  } while (a != 0)
}

What we're doing here is dividing by 2, sending % 2 to the output, then taking the resulting value and do
pushing it back into the loop, until we get down to 0, at which point we reset the value to d.

In effect, this just gives us the binary representation of the number (backward). For instance, if d were
11, the output would be 1101 repeating.

Our minimum legal value of a is 0, which results in d being set to (231*11), which in binary is the 12-bit number
100111101101. The next value up from this that will give us our desired sequence is 101010101010. This is
hex 0xAAA. So the answer is 0xAAA-(231*11).

*)

printfn "Part 1: %d" (0xAAA - (231 * 11))
