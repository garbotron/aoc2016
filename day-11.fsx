type Item = Microchip of int | Generator of int

type State = {
  Elevator: int
  Items: Map<int, Set<Item>>
}

// We can encode the entire state in 1 32-bit integer.
// There are up to 5 elements, each of which has a generator and a microchip.
// The only pieces of data we need to encode are which floor the elevator is on, plus which floors the items are on.
// Each of these only takes 2 bits (4 floors). ((5 * 2) + 1) * 2 = 22 bits total.
let encode state =
  let shift = function | Microchip x -> x * 4 + 2 | Generator x -> x * 4 + 4
  let addItems (floor, items) = items |> Seq.sumBy (fun x -> floor <<< shift x)
  state.Elevator + (state.Items |> Map.toSeq |> Seq.sumBy addItems)

let isValidItemSet items =
  let chips = items |> Seq.choose (function | Microchip x -> Some x | _ -> None)
  chips |> Seq.forall (fun x -> items |> Set.contains (Generator x))

let rec allPairs seq =
  match (seq |> Seq.tryHead) with
  | None -> Seq.empty
  | Some first ->
    let tail = seq |> Seq.tail
    Seq.concat [tail |> Seq.map (fun x -> Set [first; x]); allPairs tail]

let nextStates state =
  // We will either take 1 thing down or 2 things up.
  let items = state.Items |> Map.tryFind state.Elevator |> Option.defaultValue Set.empty
  let upwardBundles = if state.Elevator = 3 then Seq.empty else items |> allPairs
  let downwardBundles = if state.Elevator = 0 then Seq.empty else items |> Seq.map (fun x -> Set [x])
  let nextStateForBundle floor bundle =
    let floorItems = state.Items |> Map.tryFind floor |> Option.defaultValue Set.empty |> Set.union bundle
    if isValidItemSet floorItems then
      { state with
          Elevator = floor
          Items = state.Items
            |> Map.add state.Elevator (Set.difference items bundle)
            |> Map.add floor floorItems } |> Some
      else
        None
  Seq.concat [
    upwardBundles |> Seq.choose (nextStateForBundle (state.Elevator + 1))
    downwardBundles |> Seq.choose (nextStateForBundle (state.Elevator - 1)) ]

let infinity = 100 // somewhat reasonable upper-bound for short-circuiting

let run state target =
  let mutable fastestToTarget = infinity
  let mutable fastestToState = Map.empty
  let rec explore cur state =
    if cur < fastestToTarget then
      let bmp = state |> encode
      if bmp = target then
        fastestToTarget <- cur
      else
        let prevFastest = fastestToState |> Map.tryFind bmp |> Option.defaultValue infinity
        if cur < prevFastest then
          fastestToState <- fastestToState |> Map.add bmp cur
          state |> nextStates |> Seq.iter (explore (cur + 1))
  explore 0 state
  fastestToTarget

// I'm not bothering with an input parser this time - it's formatted too human-friendly.
let initialState = {
  Elevator = 0
  Items = Map [
    0, Set [Generator 0; Generator 1; Microchip 1; Generator 2; Generator 3; Microchip 3; Generator 4; Microchip 4]
    1, Set [Microchip 0; Microchip 2]
  ]
}

let targetState elemCount = {
  Elevator = 3
  Items = Map [3, [0..elemCount-1] |> List.collect (fun x -> [Microchip x; Generator x]) |> Set]
}

printfn "Part 1: %d" (run initialState (targetState 5 |> encode))

let newStuff state idx =
  let floor1 = state.Items.[0]
  { state with Items = state.Items |> Map.add 0 (floor1 |> Set.add (Microchip idx) |> Set.add (Generator idx)) }
printfn "Part 2: %d" (run ([5; 6] |> List.fold newStuff initialState) (targetState 7 |> encode))
