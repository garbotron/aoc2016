let doorID = "reyedfim"

let hashPairs idx =
  let str = sprintf "%s%u" doorID idx
  let bytes = str |> System.Text.Encoding.ASCII.GetBytes
  let md5 = bytes |> System.Security.Cryptography.MD5.HashData
  let md5hex = md5 |> Seq.map (sprintf "%02x") |> Seq.reduce (+)
  if md5hex.StartsWith "00000" then Some (md5hex.[5], md5hex.[6]) else None

Seq.initInfinite id
  |> Seq.choose hashPairs
  |> Seq.map fst
  |> Seq.take 8
  |> Seq.toArray
  |> System.String
  |> printfn "Part 1: %s"

let scanPassword passwd (loc, num) =
  if loc < '8' && not (passwd |> Map.containsKey loc) then
    passwd |> Map.add loc num
  else
    passwd

Seq.initInfinite id
  |> Seq.choose hashPairs
  |> Seq.scan scanPassword Map.empty
  |> Seq.find (fun m -> m.Count = 8)
  |> Map.toSeq
  |> Seq.sortBy fst
  |> Seq.map snd
  |> Seq.toArray
  |> System.String
  |> printfn "Part 2: %s"
