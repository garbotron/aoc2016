type Target = Register of char | Literal of int
type Opcode = Cpy | Inc | Dec | Jnz | Tgl

type Instruction = {
  Op: Opcode
  Arg1: Target
  Arg2: Target option
}

type Cpu = {
  Registers: Map<char, int>
  Program: Map<int, Instruction>
  PC: int
}

let parseInstruction (str: string) =
  let words = str.Split ' '
  let charArg i = words.[i].[0]
  let intArg i = words.[i] |> int
  let arg i = if words.[i].[0] |> System.Char.IsLetter then Register (charArg i) else Literal (intArg i)
  match words.[0] with
  | "cpy" -> { Op = Cpy; Arg1 = arg 1; Arg2 = Some (arg 2) }
  | "inc" -> { Op = Inc; Arg1 = arg 1; Arg2 = None }
  | "dec" -> { Op = Dec; Arg1 = arg 1; Arg2 = None }
  | "jnz" -> { Op = Jnz; Arg1 = arg 1; Arg2 = Some (arg 2) }
  | "tgl" -> { Op = Tgl; Arg1 = arg 1; Arg2 = None }
  | _ -> failwith "bad opcode"

let initialCpu = {
  Registers = Map.empty
  Program = System.IO.File.ReadAllLines "day-23-input.txt" |> Seq.map parseInstruction |> Seq.indexed |> Map
  PC = 0
}

let getReg c cpu = cpu.Registers |> Map.tryFind c |> Option.defaultValue 0
let setReg c v cpu = { cpu with Registers = cpu.Registers |> Map.add c v }

let resolveTarget tgt cpu =
  match tgt with
  | Register c -> cpu |> getReg c
  | Literal x -> x

let toggle instr =
  match instr.Arg2 with
  | None -> { instr with Op = if instr.Op = Inc then Dec else Inc }
  | Some _ -> { instr with Op = if instr.Op = Jnz then Cpy else Jnz }

let rec run cpu =
  let step cpu = { cpu with PC = cpu.PC + 1 }
  match cpu.Program |> Map.tryFind cpu.PC with
  | _ when cpu.PC = 2 ->
    // Accelerator for part 2: instructions 2-9 just do "a *= b" via nested loops.
    { cpu with PC = 10 } |> setReg 'a' (getReg 'a' cpu * getReg 'b' cpu) |> run

  | Some { Op = Cpy; Arg1 = tgt; Arg2 = Some (Register reg) } ->
    cpu |> setReg reg (cpu |> resolveTarget tgt) |> step |> run

  | Some { Op = Inc; Arg1 = Register reg } ->
    cpu |> setReg reg (getReg reg cpu + 1) |> step |> run

  | Some { Op = Dec; Arg1 = Register reg } ->
    cpu |> setReg reg (getReg reg cpu - 1) |> step |> run

  | Some { Op = Jnz; Arg1 = cond; Arg2 = Some jmp } ->
    (if resolveTarget cond cpu = 0 then cpu |> step else { cpu with PC = cpu.PC + resolveTarget jmp cpu }) |> run

  | Some { Op = Tgl; Arg1 = tgt } ->
    let addr = cpu.PC + resolveTarget tgt cpu
    match (cpu.Program |> Map.tryFind addr) with
    | None -> cpu |> step |> run // out of bounds
    | Some instr -> { cpu with Program = cpu.Program |> Map.add addr (toggle instr) } |> step |> run

  | None -> cpu // out of bounds
  | _ -> cpu |> step |> run // skip invalid instruction

printfn "Part 1: %d" (initialCpu |> setReg 'a' 7 |> run |> getReg 'a')
printfn "Part 2: %d" (initialCpu |> setReg 'a' 12 |> run |> getReg 'a')
