let lobby = 1, 1
let favoriteNumber = 1358

let rec countBits = function
  | 0 -> 0
  | 1 -> 1
  | n -> (n &&& 1) + countBits (n >>> 1)

let isWall (x, y) =
  if x < 0 || y < 0 then
    true
  else
    countBits ((x * x) + (3 * x) + (2 * x * y) + y + (y * y) + favoriteNumber) % 2 = 1

let neighbors (x, y) =
  seq { x + 1, y; x, y + 1; x - 1, y; x, y - 1 } |> Seq.filter (not << isWall)

let getShortestPath src dest =
  let mutable shortestPaths = Map.empty
  let mutable upperBound = 1000
  let rec explore cur pos =
    if cur < upperBound then
      if pos = dest then
        upperBound <- cur
      else
        let prev = shortestPaths |> Map.tryFind pos |> Option.defaultValue upperBound
        if cur < prev then
          shortestPaths <- shortestPaths |> Map.add pos cur
          pos |> neighbors |> Seq.iter (explore (cur + 1))
  explore 0 src
  upperBound

let findLocationsWithinSteps src stepCount =
  let mutable shortestPaths = Map.empty
  let rec explore cur pos =
    if cur <= stepCount then
      let prev = shortestPaths |> Map.tryFind pos |> Option.defaultValue (stepCount + 1)
      if cur < prev then
          shortestPaths <- shortestPaths |> Map.add pos cur
          pos |> neighbors |> Seq.iter (explore (cur + 1))
  explore 0 src
  shortestPaths |> Map.count

printfn "Part 1: %d" (getShortestPath lobby (31, 39))
printfn "Part 2: %d" (findLocationsWithinSteps lobby 50)
