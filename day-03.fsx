let validTriangle (a, b, c) = a + b > c && a + c > b && b + c > a

let parseTriangle (str: string) =
  let split = str.Split(' ', System.StringSplitOptions.RemoveEmptyEntries)
  (int split.[0], int split.[1], int split.[2])

let input1 =
  System.IO.File.ReadAllLines "day-03-input.txt"
  |> Seq.map parseTriangle

printfn "Part 1: %d" (input1 |> Seq.filter validTriangle |> Seq.length)

let input2 =
  input1
  |> Seq.chunkBySize 3
  |> Seq.collect
    (fun tris ->
      let ((a, b, c), (d, e, f), (g, h, i)) = tris.[0], tris.[1], tris.[2]
      [(a, d, g); (b, e, h); (c, f, i)])

printfn "Part 2: %d" (input2 |> Seq.filter validTriangle |> Seq.length)
