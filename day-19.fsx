type Elf = {
  ID: int
  mutable Next: Elf
  mutable Prev: Elf
}

let rec initElf = { ID = 0; Next = initElf; Prev = initElf }

let createElfRing count =
  let elves = Array.init count (fun i -> { initElf with ID = i + 1 })
  elves |> Array.iteri (fun i elf -> elf.Next <- elves.[(i + 1) % count])
  elves |> Array.iteri (fun i elf -> elf.Prev <- elves.[(i + count - 1) % count])
  elves.[0]

let rec skip count elf =
  if count = 0 then elf else elf.Next |> skip (count - 1)

let remove elf =
  let next = elf.Next
  let prev = elf.Prev
  next.Prev <- prev
  prev.Next <- next

let rec runP1 elf =
  if elf.Next.ID = elf.ID then
    elf.ID
  else
    remove elf.Next
    runP1 elf.Next

let rec runP2 ringSize elf oppositeElf =
  if ringSize = 1 then
    elf.ID
  else
    // When you remove the elf on the opposite side, you need to keep it balanced, so alternate between shifting
    // the next elf backward and the previous elf forward (starting with prev to break ties in favor of the left).
    let newOpposite = if ringSize % 2 = 0 then oppositeElf.Prev else oppositeElf.Next
    remove oppositeElf
    runP2 (ringSize - 1) elf.Next newOpposite.Next

let input = 3001330
printfn "Part 1: %d" (createElfRing input |> runP1)
printfn "Part 2: %d" (createElfRing input |> fun elf -> runP2 input elf (elf |> skip (input / 2)))
