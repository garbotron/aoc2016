open System.Text.RegularExpressions

type Disc = { ID: int; Start: int; Count: int }
type Config = { Discs: Disc list }

let foldConfigLine cfg (str: string) =
  let m = Regex.Match(str, "Disc #([0-9]+) has ([0-9]+) positions; at time=0, it is at position ([0-9]+)")
  let id = m.Groups.[1].Value |> int
  let count = m.Groups.[2].Value |> int
  let start = m.Groups.[3].Value |> int
  { cfg with Discs = { ID = id; Count = count; Start = start } :: cfg.Discs }

let input = System.IO.File.ReadAllLines "day-15-input.txt" |> Seq.fold foldConfigLine { Discs = [] }

let posAtTime disc time = (disc.Start + disc.ID + time) % disc.Count
let isValidTime cfg time = cfg.Discs |> List.forall (fun x -> posAtTime x time = 0)

printfn "Part 1: %d" (Seq.initInfinite id |> Seq.find (isValidTime input))

let extended = { input with Discs = { ID = 7; Start = 0; Count = 11 } :: input.Discs }
printfn "Part 2: %d" (Seq.initInfinite id |> Seq.find (isValidTime extended))
