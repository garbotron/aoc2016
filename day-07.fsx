let hasABBA (s: string) =
  {0..s.Length-4} |> Seq.exists (fun i -> s.[i] <> s.[i + 1] && s.[i] = s.[i + 3] && s.[i + 1] = s.[i + 2])

let getABAs (s: string) =
  s |> Seq.toList |> List.windowed 3 |> List.filter (fun x -> x.[0] <> x.[1] && x.[0] = x.[2])

let hasBAB (aba: char list) (s: string) =
  s.Contains (System.String [| aba.[1]; aba.[0]; aba.[1] |])

let breakIntoSubnets (str: string) =
  let words = str.Split [| '['; ']' |] // the input is only 1-deep and we don't need to deal with imbalanced []s
  ( words |> Seq.indexed |> Seq.filter (fun (i, _) -> i % 2 = 0) |> Seq.map snd,
    words |> Seq.indexed |> Seq.filter (fun (i, _) -> i % 2 = 1) |> Seq.map snd )

let supportsTLS (str: string) =
  let (supernet, hypernet) = breakIntoSubnets str
  supernet |> Seq.exists hasABBA && not (hypernet |> Seq.exists hasABBA)

let supportsSSL (str: string) =
  let (supernet, hypernet) = breakIntoSubnets str
  supernet |> Seq.collect getABAs |> Seq.exists (fun aba -> hypernet |> Seq.exists (hasBAB aba))

let input = System.IO.File.ReadAllLines "day-07-input.txt"
printfn "Part 1: %d" (input |> Seq.filter supportsTLS |> Seq.length)
printfn "Part 2: %d" (input |> Seq.filter supportsSSL |> Seq.length)
