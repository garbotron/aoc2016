open System.Text.RegularExpressions

type Command =
  | Rect of int * int
  | RotateRow of int * int
  | RotateColumn of int * int

let parseCommand (str: string) =
  let m1 = Regex.Match(str, "rect ([0-9]+)x([0-9]+)")
  let m2 = Regex.Match(str, "rotate row y=([0-9]+) by ([0-9]+)")
  let m3 = Regex.Match(str, "rotate column x=([0-9]+) by ([0-9]+)")
  let args (m: Match) = (int m.Groups.[1].Value, int m.Groups.[2].Value)
  match (m1.Success, m2.Success, m3.Success) with
  | true, _, _ -> Rect (args m1)
  | _, true, _ -> RotateRow (args m2)
  | _, _, true -> RotateColumn (args m3)
  | _ -> failwith "bad command"

let doRect w h state =
  let coords = [0..h-1] |> List.collect (fun y -> [0..w-1] |> List.map (fun x -> x, y))
  state |> Set.union (Set coords)

let doRotateRow ry amount state =
  state |> Set.map (fun (x, y) -> if y = ry then ((x + amount) % 50, y) else (x, y))

let doRotateColumn cx amount state =
  state |> Set.map (fun (x, y) -> if x = cx then (x, (y + amount) % 6) else (x, y))

let run state = function
  | Rect (w, h) -> state |> doRect w h
  | RotateRow (y, by) -> state |> doRotateRow y by
  | RotateColumn (x, by) -> state |> doRotateColumn x by

let printScreen screen =
  for y in [0..5] do
    for x in [0..49] do
      printf (if screen |> Set.contains (x, y) then "#" else " ")
    printfn ""

let input = System.IO.File.ReadAllLines "day-08-input.txt" |> Seq.map parseCommand

printfn "Part 1: %d" (input |> Seq.fold run Set.empty |> Set.count)
printfn "Part 2:"
input |> Seq.fold run Set.empty |> printScreen
