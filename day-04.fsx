open System.Text.RegularExpressions

let calcChecksum (room: string) =
  room
  |> Seq.toList
  |> List.filter System.Char.IsLetter
  |> List.groupBy id
  |> List.sortBy (fun (c, grp) -> -grp.Length, c)
  |> List.take 5
  |> List.map fst
  |> List.toArray
  |> System.String

let parseRoom (roomID: string) =
  let m = Regex.Match(roomID, @"([a-z\-]+)([0-9]+)\[([a-z]+)\]")
  let room = m.Groups.[1].Value
  let sector = m.Groups.[2].Value |> int
  let checksum = m.Groups.[3].Value
  if calcChecksum room = checksum then Some (room, sector) else None

let input = System.IO.File.ReadAllLines "day-04-input.txt" |> Seq.choose parseRoom

printfn "Part 1: %d" (input |> Seq.map snd |> Seq.sum)

let decryptChar sectorID = function
  | '-' -> ' '
  | x -> (int 'a') + (((int x - int 'a') + sectorID) % 26) |> char

let decrypt (room, sectorID) =
  let room = room |> Seq.map (decryptChar sectorID) |> Seq.toArray |> System.String
  room, sectorID

// Note: the actual string here was found via searching trial+error.
printfn "Part 2: %d" (input |> Seq.map decrypt |> Seq.find (fun (x, _) -> x.Contains "northpole object") |> snd)
