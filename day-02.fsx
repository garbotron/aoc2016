let moveP1 cur dir =
  match cur, dir with
  | x, 'L' when x % 3 <> 1 -> x - 1
  | x, 'R' when x % 3 <> 0 -> x + 1
  | x, 'U' when x > 3 -> x - 3
  | x, 'D' when x < 7 -> x + 3
  | _ -> cur

let moveP2 cur dir =
  match cur, dir with
  | x, 'L' when List.contains x [3; 4; 6; 7; 8; 9; 0xB; 0xC] -> x - 1
  | x, 'R' when List.contains x [2; 3; 5; 6; 7; 8; 0xA; 0xB] -> x + 1
  | x, 'U' when List.contains x [3; 0xD] -> x - 2
  | x, 'U' when List.contains x [6; 7; 8; 0xA; 0xB; 0xC] -> x - 4
  | x, 'D' when List.contains x [1; 0xB] -> x + 2
  | x, 'D' when List.contains x [2; 3; 4; 6; 7; 8] -> x + 4
  | _ -> cur

let rec getCode move cur paths =
  match paths with
  | [] -> []
  | p :: rest ->
    let key = p |> Seq.fold move cur
    key :: getCode move key rest

let formatCode code = code |> List.map (sprintf "%X") |> List.reduce (+)

let input = System.IO.File.ReadAllLines "day-02-input.txt" |> Array.toList
printfn "Part 1: %s" (input |> getCode moveP1 5 |> formatCode)
printfn "Part 2: %s" (input |> getCode moveP2 5 |> formatCode)
