let infinity = System.Int32.MaxValue

type Tile = {
  Number: int
  mutable ShortestPath: int
  mutable Links: (Tile * int) list
}

let mutable curBlankTileNumber = 0 // give a negative number to each blank tile to identify it
let foldInputLine map (y, str) =
  let foldChar map (x, c) =
    match c with
    | '.' ->
      curBlankTileNumber <- curBlankTileNumber - 1
      map |> Map.add (x, y) curBlankTileNumber
    | c when c >= '0' && c <= '9' ->
      map |> Map.add (x, y) (c |> string |> int)
    | _ -> map
  str |> Seq.indexed |> Seq.fold foldChar map

let fillInitialLinks map =
  let neighbors (x, y) = seq { x + 1, y; x, y + 1; x - 1, y; x, y - 1 }
  for (pos, tile) in map |> Map.toSeq do
    for n in neighbors pos do
      if map |> Map.containsKey n then
        tile.Links <- (map.[n], 1) :: tile.Links
  map

let initialTiles () =
  curBlankTileNumber <- 0
  System.IO.File.ReadAllLines "day-24-input.txt"
  |> Seq.indexed
  |> Seq.fold foldInputLine Map.empty
  |> Map.map (fun _ v -> { Number = v; Links = []; ShortestPath = infinity })
  |> fillInitialLinks

let rec fillShortestPaths cur tile =
  if cur < tile.ShortestPath then
    tile.ShortestPath <- cur
    tile.Links |> List.iter (fun (t, w) -> fillShortestPaths (cur + w) t)

let findTile number tiles =
  tiles |> Map.toSeq |> Seq.map snd |> Seq.find (fun x -> x.Number = number)

let getTileMap number =
  let tiles = initialTiles ()
  let start = tiles |> findTile number
  fillShortestPaths 0 start
  tiles

// For each number, map out the shortest paths to each of the other numbers.
let tileToDest =
  [0..7]
  |> Seq.map (fun i -> i, getTileMap i)
  |> Map
  |> Map.map (fun _ m -> [0..7] |> List.map (fun n -> n, findTile n m) |> Map)

// Now we can try all permuations of 0 + [1..7] and see what all of the shortest path sums are.
let rec permutations set =
  match set |> Seq.tryExactlyOne with
  | Some x -> seq {[x]}
  | None -> set |> Seq.collect (fun x -> permutations (set |> Set.remove x) |> Seq.map (fun y -> x :: y))

let totalCost permutation =
   0 :: permutation |> List.pairwise |> List.sumBy (fun (x, y) -> tileToDest.[x].[y].ShortestPath)

let backToZero lst = lst @ [0]

printfn "Part 1: %d" (Set [1..7] |> permutations |> Seq.map totalCost |> Seq.min)
printfn "Part 2: %d" (Set [1..7] |> permutations |> Seq.map (backToZero >> totalCost) |> Seq.min)
