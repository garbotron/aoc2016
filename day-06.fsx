let input = System.IO.File.ReadAllLines "day-06-input.txt"

let mostFreq seq = seq |> Seq.groupBy id |> Seq.maxBy (snd >> Seq.length) |> fst
let leastFreq seq = seq |> Seq.groupBy id |> Seq.minBy (snd >> Seq.length) |> fst

let decode freqFunc =
  // Turn the input "inside-out" by grouping by index and flattening.
  input
    |> Seq.collect Seq.indexed
    |> Seq.groupBy fst
    |> Seq.map snd
    |> Seq.map (Seq.map snd >> freqFunc)
    |> Seq.toArray
    |> System.String

printfn "Part 1: %s" (decode mostFreq)
printfn "Part 2: %s" (decode leastFreq)
