let nextRow row =
  let trap i = row |> List.tryItem i |> Option.defaultValue false
  let nextTile i =
    match (trap (i - 1), trap i, trap (i + 1)) with
    | true, true, false
    | false, true, true
    | true, false, false
    | false, false, true -> true
    | _ -> false
  [0..row.Length-1] |> List.map nextTile

let expand height firstRow =
  List.unfold
    (fun (row, remaining) ->
      if remaining = 0 then
        None
      else
        Some (row, (row |> nextRow, remaining - 1)))
    (firstRow, height)

let input =
  System.IO.File.ReadAllText("day-18-input.txt").Trim()
  |> Seq.toList
  |> List.map ((=) '^')

printfn "Part 1: %d" (input |> expand 40 |> List.collect id |> List.filter not |> List.length)
printfn "Part 2: %d" (input |> expand 400000 |> List.collect id |> List.filter not |> List.length)
