open System.Text.RegularExpressions

type Target = ToOutput of int | ToBot of int

type Config = {
  InitialValues: Map<int, int>
  BotGivesTo: Map<int, Target * Target>
}

type Bot = {
  ID: int
  Holding: int list
  Comparison: (int * int) option
}

type State = {
  Config: Config
  Bots: Map<int, Bot>
  Output: Map<int, int>
}

let foldInputLine state (str: string) =
  let m = Regex.Match(str, "value ([0-9]+) goes to bot ([0-9]+)")
  if m.Success then
    let v = m.Groups.[1].Value |> int
    let bot = m.Groups.[2].Value |> int
    { state with InitialValues = state.InitialValues |> Map.add v bot }
  else
    let m = Regex.Match(str, "bot ([0-9]+) gives low to (output|bot) ([0-9]+) and high to (output|bot) ([0-9]+)")
    let bot = m.Groups.[1].Value |> int
    let lowNum = m.Groups.[3].Value |> int
    let low = if m.Groups.[2].Value = "bot" then ToBot lowNum else ToOutput lowNum
    let highNum = m.Groups.[5].Value |> int
    let high = if m.Groups.[4].Value = "bot" then ToBot highNum else ToOutput highNum
    { state with BotGivesTo = state.BotGivesTo |> Map.add bot (low, high) }

let getBot botID state =
  state.Bots |> Map.tryFind botID |> Option.defaultValue { ID = botID; Holding = []; Comparison = None }

let initialState cfg =
  let foldHold state (v, botID) =
    let bot = state |> getBot botID
    { state with Bots = state.Bots |> Map.add botID { bot with Holding = v :: bot.Holding } }
  cfg.InitialValues |> Map.toSeq |> Seq.fold foldHold { Config = cfg; Bots = Map.empty; Output = Map.empty }

let give target v state =
  match target with
  | ToOutput x -> { state with Output = state.Output |> Map.add x v }
  | ToBot botID ->
    let bot = state |> getBot botID
    { state with Bots = state.Bots |> Map.add botID { bot with Holding = v :: bot.Holding } }

let rec run state =
  match state.Bots |> Map.toSeq |> Seq.map snd |> Seq.tryFind (fun x -> x.Holding.Length = 2) with
  | None -> state
  | Some bot ->
    let low = min bot.Holding.[0] bot.Holding.[1]
    let high = max bot.Holding.[0] bot.Holding.[1]
    let (giveLowTo, giveHighTo) = state.Config.BotGivesTo |> Map.find bot.ID
    let bot = { bot with Holding = []; Comparison = Some (low, high) }
    { state with Bots = state.Bots |> Map.add bot.ID bot }
      |> give giveLowTo low
      |> give giveHighTo high
      |> run

let input =
  System.IO.File.ReadAllLines "day-10-input.txt"
  |> Seq.fold foldInputLine { InitialValues = Map.empty; BotGivesTo = Map.empty }

let result = input |> initialState |> run

result.Bots
  |> Map.toSeq
  |> Seq.map snd
  |> Seq.find (fun x -> x.Comparison = Some (17, 61))
  |> fun x -> x.ID
  |> printfn "Part 1: %d"

printfn "Part 2: %d" (result.Output.[0] * result.Output.[1] * result.Output.[2])
