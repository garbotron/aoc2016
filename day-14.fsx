let salt = "zpqevtbw"

// Expand the map of hashes (index->hash) to include at least "count" entries.
let expandHashMap hashFunc count map =
  let curCount = map |> Map.count
  if count <= curCount then
    map
  else
    [|curCount..count-1|]
      |> Array.Parallel.map (fun i -> i, hashFunc (salt + (string i)))
      |> Array.fold (fun map (i, h) -> map |> Map.add i h) map

let keys hashFunc = seq {
  let mutable hashMap: Map<int, string> = Map.empty
  for i in Seq.initInfinite id do
    hashMap <- hashMap |> expandHashMap hashFunc (i + 1)
    let hash = hashMap.[i]
    match {0..hash.Length-3} |> Seq.tryFind (fun j -> hash.[j] = hash.[j + 1] && hash.[j] = hash.[j + 2]) with
    | None -> () // no triple found
    | Some tripleIdx ->
      let fiveTuple = System.String(hash.[tripleIdx], 5)
      hashMap <- hashMap |> expandHashMap hashFunc (i + 1001)
      if {1..1000} |> Seq.exists (fun x -> hashMap.[i + x].Contains fiveTuple) then
        yield i, hashMap.[i]
}

let md5 str =
  let bytes = str |> Seq.toArray |> Array.map byte |> System.Security.Cryptography.MD5.HashData
  bytes |> Seq.map (sprintf "%02x") |> Seq.reduce (+)

let repeat fn n = List.init n (fun _ -> fn) |> List.reduce (>>)

printfn "Part 1: %d" (keys md5 |> Seq.skip 63 |> Seq.head |> fst)
printfn "Part 2: %d" (keys (repeat md5 2017) |> Seq.skip 63 |> Seq.head |> fst)
