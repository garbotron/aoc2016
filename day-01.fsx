type Step = Left | Right | Straight
type State = { Pos: int * int; Heading: int * int }

let add (dx, dy) (x, y) = (x + dx, y + dy)
let turnLeft (hx, hy) = -hy, hx // 90 degree turn CCW
let turnRight (hx, hy) = hy, -hx // 90 degree turn CW

let rec takeStep state = function
  | Left -> { state with Heading = state.Heading |> turnLeft }
  | Right -> { state with Heading = state.Heading |> turnRight }
  | Straight -> { state with Pos = state.Pos |> add state.Heading }

let parseSteps (str: string) =
  let turn = if str.StartsWith "L" then Left else Right
  let dist = str.Substring 1 |> int
  turn :: ([1..dist] |> List.map (fun _ -> Straight))

let steps =
  System.IO.File.ReadAllText "day-01-input.txt"
  |> fun x -> x.Split ", "
  |> Seq.collect parseSteps

let dist state =
  let (x, y) = state.Pos
  abs x + abs y

let initialState = { Pos = 0, 0; Heading = 0, 1 }

printfn "Part 1: %d" (steps |> Seq.fold takeStep initialState |> dist)

let rec stepUntilRevisit state steps visited =
  match (steps |> Seq.head, steps |> Seq.tail) with
  | Straight, _ when visited |> Set.contains state.Pos -> state
  | Straight, remaining -> stepUntilRevisit (takeStep state Straight) remaining (visited |> Set.add state.Pos)
  | step, remaining -> stepUntilRevisit (takeStep state step) remaining visited

printfn "Part 2: %d" (stepUntilRevisit initialState steps Set.empty |> dist)
