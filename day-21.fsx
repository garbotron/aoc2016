type Operation =
  | SwapPositions of int * int
  | SwapLetters of char * char
  | RotateLeft of int
  | RotateRight of int
  | RotateBasedOnPosition of char
  | ReversePositions of int * int
  | MovePosition of int * int

let rev (str: string) = str.ToCharArray() |> Array.rev |> System.String

let rec scramble (str: string) (oper: Operation) =
  let len = str.Length
  match oper with
  | SwapPositions (x, y) ->
    let arr = str.ToCharArray()
    let temp = arr.[x]
    arr.[x] <- arr.[y]
    arr.[y] <- temp
    arr |> System.String
  | SwapLetters (x, y) -> str.Replace(x, '$').Replace(y, x).Replace('$', y)
  | RotateLeft n when n >= len -> scramble str (RotateLeft (n % len))
  | RotateLeft n -> str.[n..] + str.[..n-1]
  | RotateRight n when n >= len -> scramble str (RotateRight (n % len))
  | RotateRight n -> str.[len-n..] + str.[..len-n-1]
  | RotateBasedOnPosition c ->
    let idx = str.IndexOf c
    scramble str (RotateRight (idx + (if idx >= 4 then 2 else 1)))
  | ReversePositions (x, y) -> str.[0..x-1] + rev str.[x..y] + str.[y+1..]
  | MovePosition (x, y) ->
    let ch = str.[x]
    let str = str.[0..x-1] + str.[x+1..]
    str.[0..y-1] + string ch + str.[y..]

(*
  Unscrambling a "rotate based on position".

  We'll just focus on doing this for 8 characters (abcdefgh).

  The forward operation looks like this, per char index:
  [0] -> rot 1 -> [1]
  [1] -> rot 2 -> [3]
  [2] -> rot 3 -> [5]
  [3] -> rot 4 -> [7]
  [4] -> rot 6 -> [2]
  [5] -> rot 7 -> [4]
  [6] -> rot 8 -> [6]
  [7] -> rot 9 -> [0]

  This can be reversed to a left rotation to reverse the operation.
*)

let unscramble (str: string) (oper: Operation) =
  match oper with
  | SwapPositions _ -> scramble str oper // same both ways
  | SwapLetters _ -> scramble str oper // same both ways
  | RotateLeft x -> scramble str (RotateRight x)
  | RotateRight x -> scramble str (RotateLeft x)
  | ReversePositions _ -> scramble str oper // same both ways
  | MovePosition (x, y) -> scramble str (MovePosition (y, x))
  | RotateBasedOnPosition c ->
    let idxAfterRotation = str.IndexOf c
    match idxAfterRotation with
    | 0 -> scramble str (RotateLeft 9)
    | 1 -> scramble str (RotateLeft 1)
    | 2 -> scramble str (RotateLeft 6)
    | 3 -> scramble str (RotateLeft 2)
    | 4 -> scramble str (RotateLeft 7)
    | 5 -> scramble str (RotateLeft 3)
    | 6 -> scramble str (RotateLeft 8)
    | 7 -> scramble str (RotateLeft 4)
    | _ -> failwith "unexpected post-rotation index"

let parseOperation (str: string) =
  let words = str.Split ' '
  let ch i = words.[i].[0]
  let num i = words.[i] |> int
  match words.[0], words.[1] with
  | "swap", "position" -> SwapPositions (num 2, num 5)
  | "swap", "letter" -> SwapLetters (ch 2, ch 5)
  | "rotate", "left" -> RotateLeft (num 2)
  | "rotate", "right" -> RotateRight (num 2)
  | "rotate", "based" -> RotateBasedOnPosition (ch 6)
  | "reverse", "positions" -> ReversePositions (num 2, num 4)
  | "move", "position" -> MovePosition (num 2, num 5)
  | _ -> failwith "bad operation"

let input = System.IO.File.ReadAllLines "day-21-input.txt" |> Seq.map parseOperation

printfn "Part 1: %s" (input |> Seq.fold scramble "abcdefgh")
printfn "Part 2: %s" (input |> Seq.rev |> Seq.fold unscramble "fbgdceah")
