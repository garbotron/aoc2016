open System.Text.RegularExpressions

type Ver = Ver1 | Ver2

let rec decompressedLen (ver: Ver) (input: string) =
  if input = "" then
    0L
  else
    let m = Regex.Match(input, @"^\(([0-9]+)x([0-9]+)\)")
    if m.Success then
      let len = m.Groups.[1].Value |> int
      let repeat = m.Groups.[2].Value |> int64
      let substr = input.Substring (m.Index + m.Length, len)
      let substrLen =
        match ver with
        | Ver1 -> substr.Length |> int64
        | Ver2 -> substr |> decompressedLen ver
      (substrLen * repeat) + (decompressedLen ver (input.Substring (m.Index + m.Length + len)))
    else
      1L + (decompressedLen ver (input.Substring 1))

let input = System.IO.File.ReadAllText("day-09-input.txt").Trim()
printfn "Part 1: %d" (input |> decompressedLen Ver1)
printfn "Part 2: %d" (input |> decompressedLen Ver2)
