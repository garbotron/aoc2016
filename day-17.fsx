let passcode = "njfxhljp"

let openDoors path (x, y) =
  let bytes = passcode + path |> Seq.toArray |> Array.map byte |> System.Security.Cryptography.MD5.HashData
  seq {
    if y > 0 && (bytes.[0] >>> 4)    > 10uy then yield "U", (x, y - 1)
    if y < 3 && (bytes.[0] &&& 15uy) > 10uy then yield "D", (x, y + 1)
    if x > 0 && (bytes.[1] >>> 4)    > 10uy then yield "L", (x - 1, y)
    if x < 3 && (bytes.[1] &&& 15uy) > 10uy then yield "R", (x + 1, y)
  }

let rec findPath curPath pathSorter pos =
  if pos = (3, 3) then
    Some curPath
  else
    openDoors curPath pos
      |> Seq.choose (fun (c, x) -> findPath (curPath + c) pathSorter x)
      |> pathSorter String.length
      |> Seq.tryHead

printfn "Part 1: %s" (findPath "" Seq.sortBy (0, 0) |> Option.get)
printfn "Part 2: %d" (findPath "" Seq.sortByDescending (0, 0) |> Option.get |> String.length)
