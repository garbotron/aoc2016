type Target = Register of char | Literal of int

type Instruction =
  | Cpy of Target * char
  | Inc of char
  | Dec of char
  | Jnz of Target * int

type Cpu = {
  Registers: Map<char, int>
  Program: Instruction list
  PC: int
}

let parseInstruction (str: string) =
  let words = str.Split ' '
  let charArg i = words.[i].[0]
  let intArg i = words.[i] |> int
  let targetArg i = if words.[i].[0] |> System.Char.IsLetter then Register (charArg i) else Literal (intArg i)
  match words.[0] with
  | "cpy" -> Cpy (targetArg 1, charArg 2)
  | "inc" -> Inc (charArg 1)
  | "dec" -> Dec (charArg 1)
  | "jnz" -> Jnz (targetArg 1, intArg 2)
  | _ -> failwith "bad opcode"

let input = System.IO.File.ReadAllLines "day-12-input.txt" |> Array.toList |> List.map parseInstruction
let initialCpu = {
  Registers = Map.empty
  Program = input
  PC = 0
}

let getReg c cpu = cpu.Registers |> Map.tryFind c |> Option.defaultValue 0
let setReg c v cpu = { cpu with Registers = cpu.Registers |> Map.add c v }

let resolveTarget tgt cpu =
  match tgt with
  | Register c -> cpu |> getReg c
  | Literal x -> x

let rec run cpu =
  let step cpu = { cpu with PC = cpu.PC + 1 }
  match cpu.Program |> List.tryItem cpu.PC with
  | None -> cpu // out of bounds
  | Some (Cpy (tgt, reg)) -> cpu |> setReg reg (cpu |> resolveTarget tgt) |> step |> run
  | Some (Inc reg) -> cpu |> setReg reg (getReg reg cpu + 1) |> step |> run
  | Some (Dec reg) -> cpu |> setReg reg (getReg reg cpu - 1) |> step |> run
  | Some (Jnz (cond, jmp)) ->
    (if resolveTarget cond cpu = 0 then cpu |> step else { cpu with PC = cpu.PC + jmp }) |> run

printfn "Part 1: %d" (initialCpu |> run |> getReg 'a')
printfn "Part 2: %d" (initialCpu |> setReg 'c' 1 |> run |> getReg 'a')
